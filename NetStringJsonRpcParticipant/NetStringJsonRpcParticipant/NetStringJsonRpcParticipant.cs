using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace NetStringJsonRpcParticipant
{
  public class NetStringJsonRpcParticipant
  {
    private          string          _applicationId;
    private readonly NetstringParser _parser = new NetstringParser();

    private readonly Dictionary<string, TaskCompletionSource<JObject>> _pendingRequests =
      new Dictionary<string, TaskCompletionSource<JObject>>();

    private TcpClient _connectedClient;
    private int       _requestCounter = 0;

    private long   _participantCoupon;
    private string _contextSessionTag;

    #region Events

    /// <summary>
    /// Fired when a fresh set of context values are received from the CM
    /// </summary>
    public event EventHandler<IEnumerable<ContextKeyValue>> ValuesReceived;

    /// <summary>
    /// Fired when we successfully join the shared context
    /// </summary>
    public event EventHandler<string> JoinedContext;

    /// <summary>
    /// Fired when we leave the shared context for whatever reason
    /// </summary>
    public event EventHandler LeftContext;

    /// <summary>
    /// Fired when a local state change could not be written to the CM 
    /// </summary>
    public event EventHandler LocalStateDirty;

    #endregion

    public NetStringJsonRpcParticipant()
    {
      _parser.NetstringParsed += ParserOnNetstringParsed;
    }

    #region Inbound message parsing/handling

    private void ParserOnNetstringParsed(object sender, NetstringParsedHandlerArgs args)
    {
      var strMessage = Encoding.UTF8.GetString(args.Data);
      try
      {
        if (!(JsonConvert.DeserializeObject(strMessage) is JObject message))
        {
          Log.Error("Received bad json {0}", strMessage);
          return;
        }

        if (message.GetValue("method") != null)
          HandleRequest(message);
        else if (message.GetValue("result") != null)
          HandleResponse(message);
        else if (message.GetValue("error") != null)
          HandleError(message);
        else
          Log.Warn("Ignoring unexpected message type: {0}", strMessage);
      }
      catch (Exception e)
      {
        Log.Warn("Error parsing received message: {0}", strMessage);
      }
    }

    private void HandleResponse(JObject response)
    {
      Log.Debug("Received response from CM\n{0}", response);
      var                           id = response.GetValue("id")?.ToString() ?? string.Empty;
      TaskCompletionSource<JObject> tcs;
      lock (_pendingRequests)
      {
        if (_pendingRequests.TryGetValue(id, out tcs))
          _pendingRequests.Remove(id);
      }

      tcs?.SetResult(response.GetValue("result") as JObject);
    }

    private void HandleError(JObject error)
    {
      Log.Error("Received error from CM\n{0}", error);
      var id = error.GetValue("id")?.ToString() ?? string.Empty;
      if (string.IsNullOrEmpty(id)) return;
      TaskCompletionSource<JObject> tcs;
      lock (_pendingRequests)
      {
        if (_pendingRequests.TryGetValue(id, out tcs))
          _pendingRequests.Remove(id);
      }

      if (tcs != null && error.GetValue("error") is JObject jError && jError.GetValue("message") is JToken messageToken)
        tcs.SetException(new Exception(messageToken.ToString()));
    }

    private void HandleRequest(JObject request)
    {
      if (request == null) return;
      Log.Debug("Received request from CM\n{0}", request);
      var method = request.GetValue("method")?.ToString() ?? string.Empty;
      if (method.Equals("ContextParticipant.Ping", StringComparison.OrdinalIgnoreCase))
        SendMessage(
          new JObject(
            new JProperty("id", request.GetValue("id")?.ToString()),
            new JProperty("result", new JObject())
          )
        );
      else if (method.Equals("ContextParticipant.ContextChangesPending", StringComparison.OrdinalIgnoreCase))
        SendMessage(
          new JObject(
            new JProperty("id", request.GetValue("id")?.ToString()),
            new JProperty(
              "result",
              new JObject(
                new JProperty("Decision", "accept")
              )
            )
          )
        );
      else if (method.Equals("ContextParticipant.ContextChangesAccepted", StringComparison.OrdinalIgnoreCase)
               && request.GetValue("params") is JObject paramsObj
               && paramsObj.GetValue("Changes") is JObject changesObj)
      {
        var newValues = changesObj.Properties()
          .Select(p => new ContextKeyValue(p.Name, p.Value.ToString(), this))
          .ToArray();
        Log.Info("Accepted new set of changes: {0}", string.Join<ContextKeyValue>(", ", newValues));
        ValuesReceived?.Invoke(this, newValues);
      }
      else if (method.Equals("ContextParticipant.ContextChangesCancelled", StringComparison.OrdinalIgnoreCase))
      {
        Log.Warn("CM cancelled transaction");
        LocalStateDirty?.Invoke(this, EventArgs.Empty);
      }
    }

    #endregion

    #region Low level json rpc ccow requests

    private Task<JObject> JoinCommonContext(string componentId)
    {
      return SendRequest(
        new JObject(
          new JProperty("method", "ContextManager.JoinCommonContext"),
          new JProperty(
            "params",
            new JObject(
              new JProperty("ComponentId", componentId),
              new JProperty("ApplicationName", _applicationId),
              new JProperty("SendContextInTxMethods", true)
            )
          )
        )
      );
    }

    private Task<JObject> LeaveCommonContext()
    {
      if (_participantCoupon == 0) throw new Exception("No context to leave");
      return SendRequest(
        new JObject(
          new JProperty("method", "ContextManager.LeaveCommonContext"),
          new JProperty(
            "params",
            new JObject(
              new JProperty("ParticipantCoupon", _participantCoupon)
            )
          )
        )
      );
    }

    private Task<JObject> GetItemNames()
    {
      if (_participantCoupon == 0) throw new Exception("No context to get item names from");
      return SendRequest(
        new JObject(
          new JProperty("method", "ContextData.GetItemNames"),
          new JProperty(
            "params",
            new JObject(
              new JProperty("ContextCoupon", -1)
            )
          )
        )
      );
    }

    private Task<JObject> StartContextChanges()
    {
      if (_participantCoupon == 0) throw new Exception("No context to start transactions in");
      return SendRequest(
        new JObject(
          new JProperty("method", "ContextManager.StartContextChanges"),
          new JProperty(
            "params",
            new JObject(
              new JProperty("ParticipantCoupon", _participantCoupon)
            )
          )
        )
      );
    }

    private Task<JObject> EndContextChanges(long contextCoupon)
    {
      if (contextCoupon == 0) throw new Exception("No transaction to end");
      return SendRequest(
        new JObject(
          new JProperty("method", "ContextManager.EndContextChanges"),
          new JProperty(
            "params",
            new JObject(
              new JProperty("ContextCoupon", contextCoupon)
            )
          )
        )
      );
    }

    private Task<JObject> UndoContextChanges(long contextCoupon)
    {
      if (contextCoupon == 0) throw new Exception("No transaction to undo");
      return SendRequest(
        new JObject(
          new JProperty("method", "ContextManager.UndoContextChanges"),
          new JProperty(
            "params",
            new JObject(
              new JProperty("ContextCoupon", contextCoupon)
            )
          )
        )
      );
    }

    private Task<JObject> PublishChangesDecision(long contextCoupon, bool accept)
    {
      if (contextCoupon == 0) throw new Exception("No transaction to publish");
      return SendRequest(
        new JObject(
          new JProperty("method", "ContextManager.PublishChangesDecision"),
          new JProperty(
            "params",
            new JObject(
              new JProperty("ContextCoupon", contextCoupon),
              new JProperty(
                "Decision",
                accept ? "accept" : "cancel"
              )
            )
          )
        )
      );
    }

    private Task<JObject> SetItemValues(long contextCoupon, IEnumerable<string> names, IEnumerable<string> values)
    {
      if (_participantCoupon == 0) throw new Exception("No context to set values in");
      if (contextCoupon == 0) throw new Exception("No transaction to set values in");
      return SendRequest(
        new JObject(
          new JProperty("method", "ContextData.SetItemValues"),
          new JProperty(
            "params",
            new JObject(
              new JProperty("ContextCoupon", contextCoupon),
              new JProperty("ParticipantCoupon", _participantCoupon),
              new JProperty("ItemNames", new JArray(names)),
              new JProperty("ItemValues", new JArray(values))
            )
          )
        )
      );
    }

    private Task<JObject> GetItemValues(long contextCoupon, IEnumerable<string> names)
    {
      if (contextCoupon == 0) throw new Exception("No transaction to set values in");
      return SendRequest(
        new JObject(
          new JProperty("method", "ContextData.GetItemValues"),
          new JProperty(
            "params",
            new JObject(
              new JProperty("ContextCoupon", contextCoupon),
              new JProperty("ItemNames", new JArray(names))
            )
          )
        )
      );
    }

    #endregion

    #region Outbound messages

    // A request is a message that expects a reply
    private Task<JObject> SendRequest(JObject request)
    {
      var nextId = _requestCounter++.ToString();
      request["id"] = nextId;
      var tcs = new TaskCompletionSource<JObject>();

      // Set an overall timeout after which we will consider it a 'no reply' scenario
      var cs = new CancellationTokenSource(TimeSpan.FromSeconds(15));
      cs.Token.Register(
        () =>
        {
          if (tcs.Task.IsCompleted || tcs.Task.IsFaulted || tcs.Task.IsCanceled) return;
          Log.Warn("No response received for request {0}", request);
          tcs.TrySetCanceled();
        }
      );

      // Register the pending request
      lock (_pendingRequests)
      {
        _pendingRequests[nextId] = tcs;
      }

      // Send it
      SendMessage(request);
      return tcs.Task;
    }

    private void SendMessage(JObject message)
    {
      if (!(_connectedClient is TcpClient client))
        throw new Exception("Cannot send until connection to CM is established");
      try
      {
        message["jsonrpc"] = "2.0";
        Log.Debug("Sending message {0}", message);

        var reqBytes = NetstringParser.Encode(JsonConvert.SerializeObject(message));
        client.GetStream().Write(reqBytes, 0, reqBytes.Length);
      }
      catch (Exception e)
      {
        Log.Warn("Error sending message: {0} due to error {1}", message, e.Message);
      }
    }

    #endregion

    #region Connection handling

    public void Start(string applicationId, string contextSessionTag, int port)
    {
      _applicationId     = applicationId;
      _contextSessionTag = contextSessionTag;

      // Starts connect loop and upon successful connection, joins the context and retrieves the current context values
      Task.Run(
        () =>
        {
          try
          {
            // Block until we connect or fail to connect
            Log.Info("Attempting to connect to CM on port {0}", port);
            _connectedClient = new TcpClient("127.0.0.1", port);
            OnConnected();
            // Block for the duration of the connection
            ReceiveFromStream(_connectedClient.GetStream());
          }
          catch (Exception e)
          {
            Log.Error("Connection failed: {0}", e.Message);
          }
          finally
          {
            // Clean up the connection - ready to try again
            _connectedClient?.Close();
            _connectedClient   = null;
            _participantCoupon = 0;
            LeftContext?.Invoke(this, EventArgs.Empty);
            // Thread.Sleep(10000); // Wait before retrying
          }
        }
      );
    }

    private void OnConnected()
    {
      Log.Info("Connected to CM. Joining common context");
      Task.Run(
        async () =>
        {
          string componentId = null;
          if (!string.IsNullOrEmpty(_contextSessionTag))
          {
            var createResult = await CreateSession(_contextSessionTag);
            componentId = createResult.GetValue("ComponentId")?.ToString();
          }

          var joinResult = await JoinCommonContext(componentId);
          _participantCoupon = (long)joinResult.GetValue("ParticipantCoupon");
          var sessionColor = joinResult.GetValue("Color")?.ToString();
          JoinedContext?.Invoke(this, sessionColor);
          Log.Info("Joined the CM session with participant coupon {0}", _participantCoupon);
        }
      );
    }

    private Task<JObject> CreateSession(string tag)
    {
      return SendRequest(
        new JObject(
          new JProperty("method", "ContextSession.Create"),
          new JProperty(
            "params",
            new JObject(
              new JProperty("Tag", tag)
            )
          )
        )
      );
    }

    // Leaves context and disconnects 
    public void Stop()
    {
      LeaveCommonContext().ContinueWith(_ => { _connectedClient?.Close(); });
    }

    // Receives and forwards data as it arrives. Closes the connection when it fails or reaches end-of-stream
    private void ReceiveFromStream(Stream stream)
    {
      var buffer    = new byte[1024];
      var bytesRead = 0;
      while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) > 0)
      {
        _parser.Parse(buffer.Take(bytesRead).ToArray());
      }

      Log.Info("CM connection stream ended");
    }

    #endregion

    #region Higher level context context flows

    // Convenience for carrying out the full sequence of updating a single value
    public async Task UpdateContextValue(string subject, string newValue)
    {
      var startResult   = await StartContextChanges();
      var contextCoupon = (long)startResult.GetValue("ContextCoupon");
      Log.Info("Context changes started with coupon " + contextCoupon);
      // We now have a context coupon (eg transaction id) for proposing changes to the context
      try
      {
        await SetItemValues(contextCoupon, new[] { subject }, new[] { newValue });
        var endChangesResult = await EndContextChanges(contextCoupon);
        var noContinue       = (bool)endChangesResult.GetValue("NoContinue");
        if (noContinue && endChangesResult.GetValue("Responses") is JArray responseArray)
        {
          Log.Warn(
            "One or more participants objected to the change: "
            + string.Join(", ", responseArray.Select(r => r.ToString()))
          );

          noContinue = (DialogResult.Yes
                        != MessageBox.Show(
                          "One or more participants objected to the change: "
                          + string.Join(", ", responseArray.Select(r => r.ToString()))
                          + ". Continue anyway?",
                          "Warning - Context Change Rejected",
                          MessageBoxButtons.YesNo,
                          MessageBoxIcon.Warning
                        ));
        }

        // We respect the wishes of the other participants
        await PublishChangesDecision(contextCoupon, !noContinue);
      }
      catch (Exception e)
      {
        Log.Error("Transaction failed. Undoing the transaction: " + e.Message);
        await UndoContextChanges(contextCoupon);
        LocalStateDirty?.Invoke(this, EventArgs.Empty);
      }
    }

    public async Task ReadAllCurrentSubjectsAndValues()
    {
      var itemNamesResult = await GetItemNames();
      var names           = itemNamesResult.GetValue("Names") as JArray;
      if ((names?.Count ?? 0) == 0) return;
      var valuesResult     = await GetItemValues(-1, names?.Select(n => n.ToString()) ?? new string[0]);
      var contextKeyValues = new List<ContextKeyValue>();
      if (!(valuesResult.GetValue("ItemValues") is JArray jValues))
        throw new Exception(
          $"Unexpected response format for GetItemValues: {valuesResult}"
        );
      for (var i = 1; i < jValues.Count; i += 2)
      {
        contextKeyValues.Add(new ContextKeyValue(jValues[i - 1].ToString(), jValues[i].ToString(), this));
      }

      Log.Info("Got item values from context: [{0}]", string.Join(", ", contextKeyValues));
      ValuesReceived?.Invoke(this, contextKeyValues);
    }


    public async Task<JObject> PerformAction(string actionName, Dictionary<string, string> args = null)
    {
      var kvps        = args?.ToArray() ?? new KeyValuePair<string, string>[0];
      var inputNames  = kvps.Select(k => k.Key);
      var inputValues = kvps.Select(k => k.Value);

      var localActionsResult = await SendRequest(new JObject(new JProperty("method", "Registry.LocalActions")));
      Log.Info("Got local actions: {0}", localActionsResult.GetValue("Actions")?.ToString() ?? "[]");

      var targetAction = (localActionsResult.GetValue("Actions") as JArray)?.Cast<JObject>()
        .FirstOrDefault(a => a.GetValue("Name")?.ToString() == actionName);
      if (targetAction == null) throw new Exception("No action with name " + actionName + " found");
      return await SendRequest(
        new JObject(
          new JProperty("method", "ContextAction.Perform"),
          new JProperty(
            "params",
            new JObject(
              new JProperty("ActionIdentifier", targetAction.GetValue("Identifier")),
              new JProperty("InputNames", new JArray(inputNames)),
              new JProperty("InputValues", new JArray(inputValues)),
              new JProperty("ParticipantCoupon", _participantCoupon)
            )
          )
        )
      );
    }

    #endregion
  }
}