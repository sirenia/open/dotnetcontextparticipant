﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.Win32;

namespace NetStringJsonRpcParticipant
{
  public partial class Form1 : Form
  {
    private readonly NetStringJsonRpcParticipant  _participant = new NetStringJsonRpcParticipant();
    private readonly BindingList<ContextKeyValue> _bindingList = new BindingList<ContextKeyValue>();

    public Form1()
    {
      InitializeComponent();
      InitializeSomeMore();
      Log.LogLogged += OnLogLogged;
      autoScrollCBox.CheckedChanged += (sender, args) =>
      {
        if (autoScrollCBox.Checked) ScrollLogToEnd();
      };
      clearLogBn.Click            += (sender, args) => logBox.Text = string.Empty;
      joinBn.Click                += JoinBnOnClick;
      leaveBn.Click               += LeaveBnOnClick;
      refreshBn.Click             += (sender, args) => _participant.ReadAllCurrentSubjectsAndValues();
      appNameTBox.Text            =  Properties.Settings.Default.LastAppName ?? string.Empty;
      _participant.ValuesReceived += ParticipantOnValuesReceived;
      _participant.JoinedContext  += ParticipantOnJoinedContext;
      _participant.LeftContext    += ParticipantOnLeftContext;
      _participant.LocalStateDirty += (sender, args) => Log.Warn(
        "NOTE: local state is now out of sync with remote. Consider fetching fresh state from remote"
      );
      logBox.SelectionChanged += (sender, args) =>
      {
        if (logBox.SelectionLength > 0) autoScrollCBox.Checked = false;
      };
    }

    private void ParticipantOnLeftContext(object sender, EventArgs e)
    {
      BeginInvoke(
        (Action)(() =>
        {
          _bindingList.Clear();
          SetJoinState(false);
          sessionColorLbl.BackColor = SystemColors.Control;
        })
      );
    }

    private void ParticipantOnJoinedContext(object sender, string sessionColor)
    {
      BeginInvoke(
        (Action)(async () =>
        {
          // Remember app id for next run
          Properties.Settings.Default.LastAppName = appNameTBox.Text;
          Properties.Settings.Default.Save();
          SetJoinState(true);
          sessionColorLbl.BackColor = ColorTranslator.FromHtml(sessionColor);
          await _participant.ReadAllCurrentSubjectsAndValues();
        })
      );
    }

    private void SetJoinState(bool joined)
    {
      // Enabled when not joined:
      joinBn.Enabled      = !joined;
      appNameTBox.Enabled = !joined;

      // Enabled when joined:
      leaveBn.Enabled      = joined;
      dataGridView.Enabled = joined;
      refreshBn.Enabled    = joined;
      invokeBn.Enabled     = joined;
    }

    private void JoinBnOnClick(object sender, EventArgs e)
    {
      if (!string.IsNullOrEmpty(appNameTBox.Text))
        _participant.Start(appNameTBox.Text, sessionTagTextBox.Text, GetPortFromRegistry());
    }

    private void LeaveBnOnClick(object sender, EventArgs e)
    {
      _participant.Stop();
    }

    private void ActionBnOnClick(object sender, EventArgs e)
    {
      _participant.PerformAction(
        "FooFlow",
        new Dictionary<string, string>() { { "Arg1", "Val1" }, { "Arg2", "Val2" } }
      );
    }

    private void OnLogLogged(object sender, LogEvent e)
    {
      Invoke(
        (Action)(() =>
        {
          var currentCursorIndex     = logBox.SelectionStart;
          var currentSelectionLength = logBox.SelectionLength;
          var color                  = Color.Black;
          switch (e.Level)
          {
            case TraceLevel.Error:
              color = Color.Red;
              break;
            case TraceLevel.Warning:
              color = Color.Orange;
              break;
            case TraceLevel.Info:
              color = Color.Black;
              break;
            case TraceLevel.Verbose:
              color = Color.Gray;
              break;
          }

          logBox.AppendText(e.Message, color);
          if (autoScrollCBox.Checked)
            ScrollLogToEnd();
          else // Restore selection from before append
            logBox.Select(currentCursorIndex, currentSelectionLength);
        })
      );
    }

    private void ScrollLogToEnd()
    {
      logBox.Select(logBox.TextLength, 0);
      logBox.ScrollToCaret();
    }

    private void ParticipantOnValuesReceived(object sender, IEnumerable<ContextKeyValue> e)
    {
      UpdateContextValuesView(e);
    }

    private void InitializeSomeMore()
    {
      var tooltip = new ToolTip();
      tooltip.SetToolTip(appNameTBox, "Hint: Find app id in Cuesta app settings");

      _bindingList.AddingNew           += (sender, args) => args.NewObject = new ContextKeyValue("", "", _participant);
      _bindingList.AllowNew            =  true;
      dataGridView.AutoSizeColumnsMode =  DataGridViewAutoSizeColumnsMode.Fill;
      dataGridView.DataSource          =  _bindingList;
    }

    private void UpdateContextValuesView(IEnumerable<ContextKeyValue> values)
    {
      BeginInvoke(
        (Action)(() =>
        {
          _bindingList.Clear();
          foreach (var value in values)
          {
            _bindingList.Add(value);
          }
        })
      );
    }

    private int GetPortFromRegistry()
    {
      var portKey = Registry.CurrentUser.OpenSubKey(@"Software\Sirenia\Manatee\Ports");
      if (portKey == null) throw new Exception("No manatee port info in registry");
      var portString = portKey.GetValue("netstringtcp").ToString();
      return int.Parse(portString);
    }
  }
}