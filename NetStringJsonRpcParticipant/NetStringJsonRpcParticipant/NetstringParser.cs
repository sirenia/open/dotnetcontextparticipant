using System;
using System.Linq;
using System.Text;

namespace NetStringJsonRpcParticipant
{
  internal class NetstringParser
  {
    private NetstringParserState _state      = NetstringParserState.ParsingLength;
    private int                  _length     = 0;
    private byte[] _parsedData = new byte[0];

    // UTF8 values for single characters used here
    private const byte Colon     = 0x3a;
    private const byte DigitZero = 0x30;
    private const byte DigitNine = 0x39;
    private const byte Comma     = 0x2c;

    public event NetstringParsedHandler NetstringParsed;

    public void Parse(byte[] data)
    {
      for (var i = 0; i < data.Length;)
      {
        switch (_state)
        {
          case NetstringParserState.ParsingLength:
            i = ParseLength(i, data);
            break;
          case NetstringParserState.ParsingSeparator:
            i = ParseSeparator(i, data);
            break;
          case NetstringParserState.ParsingData:
            i = ParseData(i, data);
            break;
          case NetstringParserState.ParsingEnd:
            i = ParseEnd(i, data);
            break;
        }
      }
    }

    private int ParseEnd(int i, byte[] data)
    {
      if (data[i] == Comma)
      {
//        Log.Debug("Netstring message completed");
        NetstringParsed?.Invoke(this, new NetstringParsedHandlerArgs(_parsedData.ToArray()));
      }

      Reset();
      return i;
    }

    private int ParseData(int i, byte[] data)
    {
      var dataLength = Math.Min(_length, data.Length - i);
      _parsedData =  _parsedData.Concat(data.Skip(i).Take(dataLength)).ToArray();
      _length     -= dataLength;

      // Are we done yet
      if (_length == 0)
      {
        _state = NetstringParserState.ParsingEnd;
      }

      return i + dataLength;
    }

    private int ParseSeparator(int i, byte[] data)
    {
      var b = data[i];
      if (b != Colon)
      {
        // Something gone wrong -- reset
//        Log.Debug($"Illegal format for string, expected ':/{Colon:X}' but saw '{b:X}'");
        Reset();
      }
      else
      {
        _state = NetstringParserState.ParsingData;
      }

      return i + 1;
    }

    private void Reset()
    {
//      Log.Debug("Resetting parser");
      _length     = 0;
      _parsedData = new byte[0];
      _state      = NetstringParserState.ParsingLength;
    }

    private int ParseLength(int i, byte[] data)
    {
      var b = data[i];
      if (DigitZero <= b && b <= DigitNine)
      {
        // calculate the length along the way;
        _length = _length * 10 + (b - 0x30);
        return i + 1;
      }
      else
      {
        // looking for the :
        _state = NetstringParserState.ParsingSeparator;
      }

      return i;
    }
    
    public static byte[] Encode(string data)
    {
      var dataBytes = Encoding.UTF8.GetBytes(data);
      return Encoding.UTF8.GetBytes($"{dataBytes.Length}:{data},");
    }
  }
  
  internal delegate void NetstringParsedHandler(object sender, NetstringParsedHandlerArgs args);
  
  internal enum NetstringParserState
  {
    ParsingLength,
    ParsingSeparator,
    ParsingData,
    ParsingEnd
  }
  
  internal class NetstringParsedHandlerArgs
  {
    public NetstringParsedHandlerArgs(byte[] data)
    {
      Data = data;
    }

    public byte[] Data { get; }
  }
}