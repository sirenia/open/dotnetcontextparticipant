﻿namespace NetStringJsonRpcParticipant
{
  partial class Form1
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }

      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.rootPanel         = new System.Windows.Forms.TableLayoutPanel();
      this.logBox            = new System.Windows.Forms.RichTextBox();
      this.dataGridView      = new System.Windows.Forms.DataGridView();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.label4            = new System.Windows.Forms.Label();
      this.sessionColorLbl   = new System.Windows.Forms.Label();
      this.appNameTBox       = new System.Windows.Forms.TextBox();
      this.label1            = new System.Windows.Forms.Label();
      this.autoScrollCBox    = new System.Windows.Forms.CheckBox();
      this.label2            = new System.Windows.Forms.Label();
      this.flowLayoutPanel1  = new System.Windows.Forms.FlowLayoutPanel();
      this.joinBn            = new System.Windows.Forms.Button();
      this.leaveBn           = new System.Windows.Forms.Button();
      this.clearLogBn        = new System.Windows.Forms.Button();
      this.refreshBn         = new System.Windows.Forms.Button();
      this.sessionLabel      = new System.Windows.Forms.Label();
      this.sessionTagTextBox = new System.Windows.Forms.TextBox();
      this.invokeBn = new System.Windows.Forms.Button();
      this.rootPanel.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
      this.tableLayoutPanel1.SuspendLayout();
      this.flowLayoutPanel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // rootPanel
      // 
      this.rootPanel.AutoSize     = true;
      this.rootPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
      this.rootPanel.ColumnCount  = 2;
      this.rootPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.rootPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.rootPanel.Controls.Add(this.logBox, 0, 1);
      this.rootPanel.Controls.Add(this.dataGridView, 1, 0);
      this.rootPanel.Controls.Add(this.tableLayoutPanel1, 0, 0);
      this.rootPanel.Dock     = System.Windows.Forms.DockStyle.Fill;
      this.rootPanel.Location = new System.Drawing.Point(0, 0);
      this.rootPanel.Name     = "rootPanel";
      this.rootPanel.RowCount = 2;
      this.rootPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.rootPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.rootPanel.Size     = new System.Drawing.Size(815, 462);
      this.rootPanel.TabIndex = 0;
      // 
      // logBox
      // 
      this.logBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
      this.logBox.Dock      = System.Windows.Forms.DockStyle.Fill;
      this.logBox.Location  = new System.Drawing.Point(3, 165);
      this.logBox.Name      = "logBox";
      this.logBox.ReadOnly  = true;
      this.logBox.Size      = new System.Drawing.Size(401, 294);
      this.logBox.TabIndex  = 2;
      this.logBox.Text      = "";
      this.logBox.WordWrap  = false;
      // 
      // dataGridView
      // 
      this.dataGridView.AllowUserToDeleteRows       = false;
      this.dataGridView.AllowUserToResizeRows       = false;
      this.dataGridView.BackgroundColor             = System.Drawing.SystemColors.Control;
      this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridView.Dock                        = System.Windows.Forms.DockStyle.Fill;
      this.dataGridView.Enabled                     = false;
      this.dataGridView.GridColor                   = System.Drawing.SystemColors.Control;
      this.dataGridView.Location                    = new System.Drawing.Point(410, 3);
      this.dataGridView.MultiSelect                 = false;
      this.dataGridView.Name                        = "dataGridView";
      this.rootPanel.SetRowSpan(this.dataGridView, 2);
      this.dataGridView.Size     = new System.Drawing.Size(402, 456);
      this.dataGridView.TabIndex = 3;
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.AutoSize     = true;
      this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
      this.tableLayoutPanel1.ColumnCount  = 2;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel1.Controls.Add(this.label4, 0, 4);
      this.tableLayoutPanel1.Controls.Add(this.sessionColorLbl, 1, 3);
      this.tableLayoutPanel1.Controls.Add(this.appNameTBox, 1, 0);
      this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
      this.tableLayoutPanel1.Controls.Add(this.autoScrollCBox, 1, 4);
      this.tableLayoutPanel1.Controls.Add(this.label2, 0, 3);
      this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 1, 2);
      this.tableLayoutPanel1.Controls.Add(this.sessionLabel, 0, 1);
      this.tableLayoutPanel1.Controls.Add(this.sessionTagTextBox, 1, 1);
      this.tableLayoutPanel1.Dock     = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
      this.tableLayoutPanel1.Name     = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 5;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.Size     = new System.Drawing.Size(401, 156);
      this.tableLayoutPanel1.TabIndex = 4;
      // 
      // label4
      // 
      this.label4.Dock      = System.Windows.Forms.DockStyle.Top;
      this.label4.Location  = new System.Drawing.Point(3, 130);
      this.label4.Name      = "label4";
      this.label4.Size      = new System.Drawing.Size(100, 23);
      this.label4.TabIndex  = 6;
      this.label4.Text      = "Auto scroll log";
      this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // sessionColorLbl
      // 
      this.sessionColorLbl.Dock      = System.Windows.Forms.DockStyle.Top;
      this.sessionColorLbl.Location  = new System.Drawing.Point(109, 110);
      this.sessionColorLbl.Name      = "sessionColorLbl";
      this.sessionColorLbl.Size      = new System.Drawing.Size(289, 20);
      this.sessionColorLbl.TabIndex  = 5;
      this.sessionColorLbl.Text      = " ";
      this.sessionColorLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // appNameTBox
      // 
      this.appNameTBox.Dock     = System.Windows.Forms.DockStyle.Top;
      this.appNameTBox.Location = new System.Drawing.Point(109, 3);
      this.appNameTBox.Name     = "appNameTBox";
      this.appNameTBox.Size     = new System.Drawing.Size(289, 20);
      this.appNameTBox.TabIndex = 0;
      // 
      // label1
      // 
      this.label1.Dock      = System.Windows.Forms.DockStyle.Top;
      this.label1.Location  = new System.Drawing.Point(3, 0);
      this.label1.Name      = "label1";
      this.label1.Size      = new System.Drawing.Size(100, 23);
      this.label1.TabIndex  = 1;
      this.label1.Text      = "App id";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // autoScrollCBox
      // 
      this.autoScrollCBox.Checked                 = true;
      this.autoScrollCBox.CheckState              = System.Windows.Forms.CheckState.Checked;
      this.autoScrollCBox.Location                = new System.Drawing.Point(109, 133);
      this.autoScrollCBox.Name                    = "autoScrollCBox";
      this.autoScrollCBox.Size                    = new System.Drawing.Size(33, 20);
      this.autoScrollCBox.TabIndex                = 2;
      this.autoScrollCBox.UseVisualStyleBackColor = true;
      // 
      // label2
      // 
      this.label2.Location  = new System.Drawing.Point(3, 110);
      this.label2.Name      = "label2";
      this.label2.Size      = new System.Drawing.Size(100, 20);
      this.label2.TabIndex  = 3;
      this.label2.Text      = "Session color";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // flowLayoutPanel1
      // 
      this.flowLayoutPanel1.AutoSize     = true;
      this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
      this.flowLayoutPanel1.Controls.Add(this.joinBn);
      this.flowLayoutPanel1.Controls.Add(this.leaveBn);
      this.flowLayoutPanel1.Controls.Add(this.clearLogBn);
      this.flowLayoutPanel1.Controls.Add(this.refreshBn);
      this.flowLayoutPanel1.Controls.Add(this.invokeBn);
      this.flowLayoutPanel1.Dock     = System.Windows.Forms.DockStyle.Top;
      this.flowLayoutPanel1.Location = new System.Drawing.Point(109, 49);
      this.flowLayoutPanel1.Name     = "flowLayoutPanel1";
      this.flowLayoutPanel1.Size     = new System.Drawing.Size(289, 58);
      this.flowLayoutPanel1.TabIndex = 4;
      // 
      // joinBn
      // 
      this.joinBn.Location                = new System.Drawing.Point(3, 3);
      this.joinBn.Name                    = "joinBn";
      this.joinBn.Size                    = new System.Drawing.Size(75, 23);
      this.joinBn.TabIndex                = 0;
      this.joinBn.Text                    = "Join";
      this.joinBn.UseVisualStyleBackColor = true;
      // 
      // leaveBn
      // 
      this.leaveBn.Enabled                 = false;
      this.leaveBn.Location                = new System.Drawing.Point(84, 3);
      this.leaveBn.Name                    = "leaveBn";
      this.leaveBn.Size                    = new System.Drawing.Size(75, 23);
      this.leaveBn.TabIndex                = 1;
      this.leaveBn.Text                    = "Leave";
      this.leaveBn.UseVisualStyleBackColor = true;
      // 
      // clearLogBn
      // 
      this.clearLogBn.Location                = new System.Drawing.Point(165, 3);
      this.clearLogBn.Name                    = "clearLogBn";
      this.clearLogBn.Size                    = new System.Drawing.Size(75, 23);
      this.clearLogBn.TabIndex                = 2;
      this.clearLogBn.Text                    = "Clear log";
      this.clearLogBn.UseVisualStyleBackColor = true;
      // 
      // refreshBn
      // 
      this.refreshBn.Enabled                 = false;
      this.refreshBn.Location                = new System.Drawing.Point(3, 32);
      this.refreshBn.Name                    = "refreshBn";
      this.refreshBn.Size                    = new System.Drawing.Size(104, 23);
      this.refreshBn.TabIndex                = 3;
      this.refreshBn.Text                    = "Refresh from CM";
      this.refreshBn.UseVisualStyleBackColor = true;
      // 
      // sessionLabel
      // 
      this.sessionLabel.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                                                                         | System.Windows.Forms.AnchorStyles.Left)
                                                                        | System.Windows.Forms.AnchorStyles.Right)));
      this.sessionLabel.AutoSize  = true;
      this.sessionLabel.Location  = new System.Drawing.Point(3, 26);
      this.sessionLabel.Name      = "sessionLabel";
      this.sessionLabel.Size      = new System.Drawing.Size(100, 20);
      this.sessionLabel.TabIndex  = 7;
      this.sessionLabel.Text      = "Session tag";
      this.sessionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // sessionTagTextBox
      // 
      this.sessionTagTextBox.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                                                                              | System.Windows.Forms.AnchorStyles.Left)
                                                                             | System.Windows.Forms.AnchorStyles.Right)));
      this.sessionTagTextBox.Location = new System.Drawing.Point(109, 29);
      this.sessionTagTextBox.Name     = "sessionTagTextBox";
      this.sessionTagTextBox.Size     = new System.Drawing.Size(289, 20);
      this.sessionTagTextBox.TabIndex = 8;
      // 
      // invokeButton
      // 
      this.invokeBn.Enabled = false;
      this.invokeBn.Location = new System.Drawing.Point(113, 32);
      this.invokeBn.Name = "invokeBn";
      this.invokeBn.Size = new System.Drawing.Size(75, 23);
      this.invokeBn.TabIndex = 4;
      this.invokeBn.Text = "FooFlow";
      this.invokeBn.UseVisualStyleBackColor = true;
      this.invokeBn.Click += new System.EventHandler(this.ActionBnOnClick);
      // 
      // Form1
      // 
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
      this.ClientSize    = new System.Drawing.Size(815, 462);
      this.Controls.Add(this.rootPanel);
      this.Name = "Form1";
      this.Text = "NetStringJsonRpcParticipant Demo";
      this.rootPanel.ResumeLayout(false);
      this.rootPanel.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel1.PerformLayout();
      this.flowLayoutPanel1.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    private System.Windows.Forms.Button invokeBn;

    private System.Windows.Forms.Button button1;

    private System.Windows.Forms.TextBox sessionTagTextBox;

    private System.Windows.Forms.Label sessionLabel;

    private System.Windows.Forms.Button refreshBn;

    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label sessionColorLbl;

    #endregion

    private System.Windows.Forms.RichTextBox logBox;
    private System.Windows.Forms.TableLayoutPanel rootPanel;
    private System.Windows.Forms.DataGridView dataGridView;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox appNameTBox;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    private System.Windows.Forms.CheckBox autoScrollCBox;
    private System.Windows.Forms.Button leaveBn;
    private System.Windows.Forms.Button joinBn;
    private System.Windows.Forms.Button clearLogBn;
  }
}