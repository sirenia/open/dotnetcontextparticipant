using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace NetStringJsonRpcParticipant
{
  public static class Log
  {
    public static event EventHandler<LogEvent> LogLogged;

    private static readonly object LogMutex = new object();

    public static void Debug(string format, params object[] args)
    {
      Write(format, TraceLevel.Verbose, args);
    }

    public static void Info(string format, params object[] args)
    {
      Write(format, TraceLevel.Info, args);
    }

    public static void Warn(string format, params object[] args)
    {
      Write(format, TraceLevel.Warning, args);
    }

    public static void Error(string format, params object[] args)
    {
      Write(format, TraceLevel.Error, args);
    }

    public static void Write(string format, TraceLevel level, params object[] args)
    {
      Task.Run(
        () =>
        {
          lock (LogMutex)
          {
            var message = $"{DateTime.Now:hh:mm:ss:fff}: {string.Format(format, args)}\n";
            LogLogged?.Invoke(null, new LogEvent { Message = message, Level = level });
            if (Debugger.IsAttached)
              Debugger.Log(0, string.Empty, message);
            else
            {
              Console.Out.Write(message);
              Console.Out.Flush();
            }
          }
        }
      );
    }
  }

  public class LogEvent
  {
    public string Message { get; set; }
    public TraceLevel Level { get; set; }
  }
}