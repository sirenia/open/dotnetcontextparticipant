using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace NetStringJsonRpcParticipant
{
  public class ContextKeyValue
  {
    private readonly NetStringJsonRpcParticipant _participant;
    private          string                      _value;
    private          string                      _subject;

    public string Subject
    {
      get => _subject;
      set
      {
        _subject = value;
        SaveToContextManager();
      }
    }

    public string Value
    {
      get => _value;
      set
      {
        var oldValue = _value;
        _value = value;
        Log.Info("Updating value of {0}: {1} -> {2}", Subject, oldValue, value);
        SaveToContextManager();
      }
    }

    private void SaveToContextManager()
    {
      if (string.IsNullOrEmpty(Subject)) return;
      _participant.UpdateContextValue(Subject, _value)
        .ContinueWith((Task _) => { Log.Info("Item value update complete for subject " + Subject); });
    }

    public ContextKeyValue(string subject, string value, NetStringJsonRpcParticipant participant)
    {
      _participant = participant;
      _subject     = subject;
      _value       = value;
    }

    public override string ToString()
    {
      return $"{Subject}={Value}";
    }
  }
}